#!/usr/bin/env python
import csv
import json
from optparse import OptionParser


def main():
    '''
    Simple function for reading a csv file
    and writing to a json file. Function
    takes two cli arguments:
    -t <target file>
    -d <destination file>
    '''
    parser = OptionParser()
    parser.add_option("-t", help="target file, csv format",)
    parser.add_option("-d", help="destination file, json format",)
    (options, args) = parser.parse_args()
    if options.t and options.d:
        csvfile = options.t
        print "parsing " + options.t
        f = csv.DictReader(open(csvfile, 'rb'))
        out = json.dumps([row for row in f], indent=4)
        jsonfile = open(options.d, 'w')
        jsonfile.write(out)
        print "writing " + options.d
    else:
        print "You must specify target file (-t) and destination file (-f)"


if __name__ == "__main__":
    main()
