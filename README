Zymeworks Interview Solution
============================

Notes on methodology
--------------------

1. Extracting data from Excel file.

Parsing data driectly from excel files is notoriously tricky as it very much depends 
on how the data is arranged within the file by the user. In my experience it is often 
easier to enforce a strict template format on spreadsheet data than wrtie an algorithm
to catch all edge cases. For this reason I elected to dump the data manually to .csv, 
via google spreadsheets. This results in a common data format which is simpler to parse.


2. Converting from CSV to JSON

Python has a module in the standard library for handling CSV operations, 
so not much code is needed to read the contents of the csv dumps and converting it 
to JSON is done using the json module. This is straightforward as Python dicts and json 
objects are practically identical in structure. I chose not to do much data transformation 
at this step in order that this python module exists as a general csv to json convertor and 
doesn't have to make too many assumptions about the data.


3. Choosing a Javascript charting library

I initially expected to use D3.js for this part of the problem. However, after looking at 
the dataset and considering my available time, it seemed D3.js may be overkill. 
The two datasets shared a common X-axis, and I had decided to plot all lines on the 
same chart colored by Group. In this case all I really needed to do was read in a list of 
x and y values and plot a line chart. So I decided to use a fork of the Chart.js
library (https://github.com/FVANCOP/ChartNew.js).

The solution is fairly extensible if datasets share the x-axis, and additional 
json files can simply be added to the intial `files` array. JQuery was also included with 
the solution. Even though this wasn't necessary, JQuery makes AJAX calls and manipulation 
of the DOM simpler.


4. Limitations of this approach

Firstly, there are a number of assumptions inherint in my solution. For example, 
the python script doesn't handle any header data as I knew my csv dumps were seperated
into files by group. There is also no checking of 'valid' datatypes 
(e.g. numbers vs strings). The javscript module also assumes a common x-axis, and would
need to be refactored if this changed. It also assumes that any values which cannot be 
parsed to integers are the key titles. 

Secondly, given the domain of the problem and time available, there is very little error 
handling in this solution. If this were to be used in production I would expect to include
hadnling of unexpected datatypes, outlying values and corrupt files.